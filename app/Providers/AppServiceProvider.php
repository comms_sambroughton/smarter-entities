<?php

namespace App\Providers;

use App\Repositories\Base\BaseRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static $repoMap = [];

    public static function repoMap(array $map = null, $merge = true)
    {
        if (is_array($map)) {
            static::$repoMap =
                $merge && static::$repoMap
                    ? $map + static::$repoMap
                    : $map;
        }

        return static::$repoMap;
    }

    public static function getContainerFor($repo)
    {
        if (! array_has(static::$repoMap, $repo)) throw new \Exception("Unregistered repo [{$repo}] - please register using AppServiceProvider::repoMap() in a service provider.");

        return static::$repoMap[$repo];
    }
}

<?php

namespace App\Query;

use App\Entities\BaseEntity;
use App\Repositories\Base\BaseRepositoryContract;
use App\Traits\BuildsEntities;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;

class Builder
{
    use BuildsEntities;

    protected $wheres = [];
    protected $withTrashed = false;
    private $repo;
    private $entity;
    protected $passthru = [
        'take',
        'where',
        'with',
        'has'
    ];

    public function __construct(BaseEntity $entity, BaseRepositoryContract $repo)
    {
        $this->repo = $repo;
        $this->entity = $entity;
    }

    public function first()
    {
        $this->take(1);
        $result = $this->get();
        return $result->first();
    }

    public function all()
    {
        return collect($this->get());
    }

    public function find($key)
    {
        $this->where($this->entity->getKeyName(), $key);
        $result = $this->get();
        return $result->first();
    }

    public function withTrashed()
    {
        $this->withTrashed = true;
        return $this;
    }

    public function get()
    {
        if (!$this->withTrashed) $this->where('deleted_at', '=', null);
        $result = $this->repo->get();

        if (!$result) return null;
        return $result instanceof Collection
            ? $this->buildEntities($result)
            : $this->buildEntity($result);
    }

    public function __call($method, $parameters)
    {
        if (in_array($method, $this->passthru)) {
            $this->repo->$method(...$parameters);
            return $this;
        }
        throw new \Exception('Method ['.$method.'] does not exist on '.static::class);
    }
}
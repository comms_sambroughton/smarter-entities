<?php

namespace App\Enities\Concerns;

use App\Entities\BaseEntity;
use App\Repositories\Base\BaseRepositoryContract;
use App\Traits\BuildsEntities;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Entities\Relations\Builder as RelationBuilder;

trait InteractsWithRepository
{
    public static $softDeletes = false;
    public $exists = false;
    public function save()
    {
        $this->updateRepoAttributes();
        $this->updateRepoRelations();

        // wrap in try-catch
        $this->repo->save();

        $this->unsavedAddedRelations = [];

        if (!$this->exists) $this->initialise();
    }

    public function delete()
    {
        static::$softDeletes
            ? $this->softDelete()
            : $this->repo->delete();
        $this->exists = false;
    }

    protected function softDelete()
    {
        $this->update(['deleted_at' => Carbon::now()]);
    }

    public function restore()
    {
        $this->update(['deleted_at' => null]);
        $this->exists = true;
    }

    protected function updateRepoAttributes()
    {
        $this->repo->setAttributes($this->attributes);
    }

    protected function updateRepoRelations()
    {
        foreach ($this->unsavedAddedRelations as $key => $value) {
            if (!is_array($value)) return $this->repo->associateRelation($key, $value->repo);

            return $this->repo->attachRelations($key, collect($value)->mapWithKeys(function ($entity) {
                return [$entity->getKey() => $entity->pivot->toArray()];
            }));
        }

        foreach ($this->unsavedRemovedRelations as $key => $value) {
            is_array($value)
                ? $this->repo->detachRelations($key, collect($value)->map(function ($entity) {
                return $entity->getKey();
            }))
                : $this->repo->dissociateRelation($key);
        }
    }

    protected function initialise()
    {
        $this->setAttribute($this->getKeyName(), $this->repo->getKey());
        $this->exists = true;
    }

    public function __call($method, $parameters)
    {
        if ($this->repo->hasRelation($method)) {
            return new RelationBuilder($method, $this, $this->repo);
        }

        return $this->newQuery()->$method(...$parameters);
    }
}
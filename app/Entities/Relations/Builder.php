<?php

namespace App\Entities\Relations;

use App\Entities\BaseEntity;
use App\Providers\AppServiceProvider;
use App\Repositories\Base\BaseRepositoryContract;

class Builder
{
    private $entity;
    private $name;
    private $repo;

    public function __construct(string $relation, BaseEntity $entity, BaseRepositoryContract $repo)
    {
        $this->entity = $entity;
        $this->name = $relation;
        $this->repo = $repo;
    }

    public function associate(BaseEntity $relation)
    {
        $this->entity->relations[$this->name] = $relation;
        $this->entity->unsavedAddedRelations[$this->name] = $relation;
        return $this->entity;
    }

    public function dissociate()
    {
        $this->entity->relations[$this->name] = null;
        $this->entity->unsavedRemovedRelations[$this->name] = true;
        return $this->entity;
    }

    public function attach(BaseEntity $relation, array $pivotData = [])
    {
        $relation->setAttribute('pivot', new Pivot($pivotData));

        if (!isset($this->entity->relations[$this->name]))
            $this->entity->relations[$this->name] = collect();

        $this->entity->relations[$this->name][] = $relation;
        $this->entity->unsavedAddedRelations[$this->name][] = $relation;
        return $this->entity;
    }

    public function detach(BaseEntity $relation)
    {
        $relationKey = $this->entity->relations[$this->name]->search($relation);
        unset($this->entity->relations[$this->name][$relationKey]);
        $this->entity->unsavedRemovedRelations[$this->name][] = $relation;
        return $this->entity;
    }

    public function create(array $attributes = [])
    {
        $relationRepo = $this->repo->createRelation($this->name, $attributes);
        $relationEntityClass = AppServiceProvider::getContainerFor(get_class($relationRepo));
        return app()->make($relationEntityClass)->buildEntity($relationRepo);
    }
}
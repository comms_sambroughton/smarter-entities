<?php

namespace App\Traits;

use App\Entities\Relations\Pivot;
use App\Providers\AppServiceProvider;
use App\Repositories\Base\BaseRepositoryContract;
use Illuminate\Support\Collection;

trait BuildsEntities
{
    public function buildEntities(Collection $repos)
    {
        return $repos->map(function ($repo) {
            return $this->buildEntity($repo);
        })->filter();
    }

    public function buildEntity(BaseRepositoryContract $repo)
    {
        $entityClass = AppServiceProvider::getContainerFor(get_class($repo));
        $entity = new $entityClass($repo);
        $entity->setAttributes($repo->getAttributes());
        $entity->setAttributes(['pivot' => new Pivot($repo->getPivotData())]);
        $entity->exists = true;
        return $entity;
    }
}
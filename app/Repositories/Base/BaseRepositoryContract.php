<?php

namespace App\Repositories\Base;

interface BaseRepositoryContract
{
    public function setAttributes(array $attributes = []): void;

}
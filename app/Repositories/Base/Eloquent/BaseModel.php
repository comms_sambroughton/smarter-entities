<?php

namespace App\Repositories\Base\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

abstract class BaseModel extends Model
{
    protected $guarded = [];

    public function hasRelation(string $name)
    {
        return !method_exists(self::class, $name)
            && method_exists(static::class, $name);
    }
}
<?php

namespace App\Repositories\Base\Eloquent;

use App\Providers\AppServiceProvider;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Collection;

abstract class BaseRepository
{
    private $model;
    protected $query;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
        $this->query = $model->newInstance();
    }

    public function setAttributes(array $attributes = []): void
    {
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getAttributes()
    {
        return $this->model->getAttributes();
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        $this->model->delete();
        $this->model = $this->model->newInstance();
    }

    public function __set($key, $value)
    {
        $this->model->$key = $value;
    }

    public function getKey()
    {
        return $this->model->getKey();
    }

    protected function wrapModel(BaseModel $model)
    {
        $repo = AppServiceProvider::getContainerFor(get_class($model));
        return new $repo($model);
    }

    protected function wrapModels(Collection $models)
    {
        return $models->map(function ($model) {
            return $this->wrapModel($model);
        });
    }

    public function take($number)
    {
        $this->query = $this->query->take($number);
    }

    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        $this->query = $this->query->where($column, $operator, $value, $boolean);
    }

    public function with(string $relation)
    {
        $this->query = $this->query->with($relation);
    }

    public function get()
    {
        $queryResult = $this->query->get();
        $this->query = $this->model;
        return $queryResult instanceof Collection
            ? $this->wrapModels($queryResult)
            : $this->wrapModel($queryResult);
    }

    public function hasRelation(string $name)
    {
        return $this->model->hasRelation($name);
    }

    public function getRelation(string $key)
    {
        $relation = $this->model->$key;
        if (!$relation) return null;
        return $relation instanceof Collection
            ? $this->wrapModels($relation)
            : $this->wrapModel($relation);
    }

    public function createRelation(string $key, array $attributes = [])
    {
        $model = $this->model->$key()->create($attributes);
        if ($this->model->$key() instanceof BelongsTo) {
            $this->model->$key()->associate($model)->save();
        }
        $repo = AppServiceProvider::getContainerFor(get_class($model));
        return new $repo($model);
    }

    public function associateRelation($name, $relation)
    {
        $this->model->{$name}()->associate($relation->model);
    }

    public function dissociateRelation($name)
    {
        $this->model->{$name}()->dissociate();
    }

    public function attachRelations($name, $relations)
    {
        $this->model->{$name}()->attach($relations);
    }

    public function detachRelations($name, $relationIds)
    {
        $this->model->{$name}()->detach($relationIds);
    }

    public function getPivotData()
    {
        $relations = $this->model->getRelations();
        return isset($relations['pivot'])
            ? $relations['pivot']->toArray()
            : [];
    }
}
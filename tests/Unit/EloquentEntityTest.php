<?php

namespace Tests\Unit;

use App\Entities\BaseEntity;
use App\Entities\Relations\Pivot;
use App\Providers\AppServiceProvider;
use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Base\Eloquent\BaseModel;
use App\Repositories\Base\Eloquent\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\EloquentEntityTestEntity as Entity;
use Tests\Unit\EloquentEntityTestRepo as Repo;
use Tests\Unit\EloquentEntityTestModel as Model;
use Tests\Unit\EloquentEntityTestEntityBelongsTo as EntityBelongsTo;
use Tests\Unit\EloquentEntityTestRepoBelongsTo as RepoBelongsTo;
use Tests\Unit\EloquentEntityTestModelBelongsTo as ModelBelongsTo;
use Tests\Unit\EloquentEntityTestEntityBelongsToMany as EntityBelongsToMany;
use Tests\Unit\EloquentEntityTestRepoBelongsToMany as RepoBelongsToMany;
use Tests\Unit\EloquentEntityTestModelBelongsToMany as ModelBelongsToMany;

class EloquentEntityTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->createTables([
            'tests' => ['name' => 'string'],
            'belongs_tos' => ['name' => 'string', 'owner_id' => ['integer', 'nullable']],
            'pivot_table' => ['owner_id' => ['integer', 'nullable'], 'owned_id' => ['integer', 'nullable'], 'name' => ['string', 'nullable']]
        ]);
        AppServiceProvider::repoMap([
            Repo::class => Entity::class,
            RepoBelongsTo::class => EntityBelongsTo::class,
            RepoBelongsToMany::class => EntityBelongsToMany::class,
            Model::class => Repo::class,
            ModelBelongsTo::class => RepoBelongsTo::class,
            ModelBelongsToMany::class => RepoBelongsToMany::class,
        ]);
    }

    /** @test */
    public function it_can_create_an_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $this->assertTrue($foo->exists);
        $this->assertInstanceOf(Entity::class, $foo);
        $this->assertEquals('Foo', $foo->name);
        $this->assertEquals(1, $foo->id);
    }

    /** @test */
    public function it_can_create_an_entity_using_make_and_save()
    {
        $foo = Entity::make();
        $foo->name = 'Foo';
        $foo->save();

        $this->assertCount(1, Entity::all());
        $this->assertInstanceOf(Entity::class, $foo);
        $this->assertInstanceOf(Entity::class, Entity::first());
        $this->assertEquals('Foo', Entity::first()->name);
        $this->assertEquals($foo->id, Entity::first()->id);
    }

    /** @test */
    public function it_can_update_an_entity_after_creation()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $foo->name = 'Updated';
        $foo->save();
        $this->assertEquals('Updated', Entity::first()->name);

        $foo->update(['name' => 'Updated again']);
        $this->assertEquals('Updated again', Entity::first()->name);
    }

    /** @test */
    public function it_can_delete_an_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $foo->delete();

        $this->assertCount(0, Entity::all());
        $this->assertFalse($foo->exists);
    }

    /** @test */
    public function it_can_soft_delete_an_entity()
    {
        Entity::$softDeletes = true;
        $foo = Entity::create(['name' => 'Foo']);

        $foo->delete();

        $this->assertCount(0, Entity::all());
        $this->assertFalse($foo->exists);
        $this->assertCount(1, Entity::withTrashed()->all());
        Entity::$softDeletes = false;
    }

    /** @test */
    public function it_can_be_restored_after_being_soft_deleted()
    {
        Entity::$softDeletes = true;
        $foo = Entity::create(['name' => 'Foo']);

        $foo->delete();
        $foo->restore();

        $this->assertCount(1, Entity::all());
        $this->assertTrue($foo->exists);
        $this->assertEquals('Foo', $foo->name);
        $this->assertEquals($foo->id, Entity::first()->id);

        Entity::$softDeletes = false;
    }

    /** @test */
    public function it_can_associate_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owner()->associate($bar)->save();
        $retrievedFoo = EntityBelongsTo::find($foo->id);

        $this->assertEquals('Bar', $foo->owner->name);
        $this->assertEquals('Bar', $retrievedFoo->owner->name);
        $this->assertInstanceOf(Entity::class, $foo->owner);
        $this->assertInstanceOf(Entity::class, $retrievedFoo->owner);
    }

    /** @test */
    public function it_can_dissociate_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $foo->owner()->associate($bar)->save();

        $foo->owner()->dissociate()->save();

        $this->assertNull($foo->owner);
        $this->assertNull(EntityBelongsTo::find($foo->id)->owner);
    }

    /** @test */
    public function it_can_attach_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);

        $foo->owners()->attach($bar);
        $foo->owners()->attach($baz);
        $foo->save();

        $retrievedFoo = EntityBelongsToMany::first();
        $this->assertCount(2, $foo->owners);
        $this->assertCount(2, $retrievedFoo->owners);
        $this->assertEquals('Bar', $retrievedFoo->owners->first()->name);
        $this->assertEquals('Baz', $retrievedFoo->owners->last()->name);
        $this->assertInstanceOf(Collection::class, $foo->owners);
        $this->assertInstanceOf(Entity::class, $foo->owners->first());
        $this->assertInstanceOf(Entity::class, $retrievedFoo->owners->first());
    }

    /** @test */
    public function it_can_attach_a_relation_with_pivot_data()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);

        $foo->owners()->attach($bar, ['name' => 'FooBar'])->save();
        $retrievedFoo = EntityBelongsToMany::first();

        $this->assertInstanceOf(Pivot::class, $foo->owners->first()->pivot);
        $this->assertInstanceOf(Pivot::class, $retrievedFoo->owners->first()->pivot);
        $this->assertEquals('FooBar', $foo->owners->first()->pivot->name);
        $this->assertEquals('FooBar', $retrievedFoo->owners->first()->pivot->name);
    }

    /** @test */
    public function it_can_detach_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);

        $foo->owners()->attach($bar)->save();
        $foo->owners()->attach($baz)->save();
        $foo->owners()->detach($bar)->save();

        $retrievedFoo = EntityBelongsToMany::first();
        $this->assertCount(1, $foo->owners);
        $this->assertCount(1, $retrievedFoo->owners);
        $this->assertEquals('Baz', $retrievedFoo->owners->first()->name);
    }

    /** @test */
    public function it_can_create_a_relation_through_a_relation()
    {
        $foo = EntityBelongsTo::create(['name' => 'Foo']);
        $bar = $foo->owner()->create(['name' => 'Bar']);

        $this->assertEquals('Bar', $foo->owner->name);
        $this->assertEquals('Bar', EntityBelongsTo::find($foo->id)->owner->name);
    }

    /** @test */
    public function it_can_retrieve_the_first_saved_entity()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $first = Entity::first();

        $this->assertInstanceOf(Entity::class, $first);
        $this->assertEquals($foo->id, $first->id);
        $this->assertEquals('Foo', $first->name);
        $this->assertTrue($first->exists);
    }

    /** @test */
    public function it_can_retrieve_all_saved_entities()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $all = Entity::all();

        $this->assertInstanceOf(Collection::class, $all);
        $this->assertInstanceOf(Entity::class, $all->first());
        $this->assertInstanceOf(Entity::class, $all->last());
        $this->assertEquals('Foo', $all->first()->name);
        $this->assertEquals('Bar', $all->last()->name);
    }

    /** @test */
    public function it_can_retrieve_an_entity_by_its_key()
    {
        $foo = Entity::create(['name' => 'Foo']);

        $find = Entity::find($foo->getKey());

        $this->assertInstanceOf(Entity::class, $find);
        $this->assertEquals('Foo', $find->name);
    }

    /** @test */
    public function it_can_retrieve_using_a_where_clause()
    {
        $foo = Entity::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $where = Entity::where('name', 'Foo')->get();

        $this->assertInstanceOf(Collection::class, $where);
        $this->assertCount(1, $where);
        $this->assertInstanceOf(Entity::class, $where->first());
        $this->assertEquals('Foo', $where->first()->name);
    }

    /** @test */
    public function it_can_eager_load_relations()
    {
        $foo = EntityBelongsToMany::create(['name' => 'Foo']);
        $bar = Entity::create(['name' => 'Bar']);
        $baz = Entity::create(['name' => 'Baz']);
        $foo->owners()->attach($bar);
        $foo->owners()->attach($baz);
        $foo->save();

        $retrievedFoo = EntityBelongsToMany::with('owners')->first();

        DB::enableQueryLog();
        $this->assertCount(2, $retrievedFoo->owners);
        $this->assertCount(0, DB::getQueryLog());

        DB::disableQueryLog();
    }
}

class EloquentEntityTestEntity extends BaseEntity
{
    public function __construct(Repo $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepo extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModel extends BaseModel
{
    protected $table = 'tests';
}

class EloquentEntityTestEntityBelongsTo extends BaseEntity
{
    public function __construct(RepoBelongsTo $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepoBelongsTo extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(ModelBelongsTo $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModelBelongsTo extends BaseModel
{
    protected $table = 'belongs_tos';

    public function owner()
    {
        return $this->belongsTo(Model::class, 'owner_id');
    }
}

class EloquentEntityTestEntityBelongsToMany extends BaseEntity
{
    public function __construct(RepoBelongsToMany $repo)
    {
        parent::__construct($repo);
    }

}

class EloquentEntityTestRepoBelongsToMany extends BaseRepository implements BaseRepositoryContract
{
    public function __construct(ModelBelongsToMany $model)
    {
        parent::__construct($model);
    }
}

class EloquentEntityTestModelBelongsToMany extends BaseModel
{
    protected $table = 'tests';

    public function owners()
    {
        return $this->belongsToMany(Model::class, 'pivot_table', 'owner_id', 'owned_id')
            ->withPivot('name');
    }
}
